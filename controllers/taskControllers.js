const Task = require('../models/taskSchema')

// GET ALL TASK

module.exports.getAllTasks = () => {
    return Task.find({}).then(result => {
        return result
    })
}

// CREATING NEW TASK
                        // req body dev defined
module.exports.createTask = (reqBody) => {
        let newTask = new Task({
            name: reqBody.name

        })
                                    // then() method, task first before error, unlike save()
                                    // .then() getting the promise
                    // save() saves in the newTask
        return newTask.save().then((task, err) => {
            if(err) {
                console.log(err)
                return false
            }else{
                return task // returning the task that was saved in save()
            }
        })

}


// delete task


module.exports.deleteTask = (taskID) => {

    return Task.findOneAndDelete(taskID).then((removedTask, err) => {

        if(err){
            console.log(err)
            return false
        } else {
            return removedTask
        }
    })
}

// update task controller

module.exports.updateTask = (taskID, newContent) => {
    return Task.findById(taskID).then((result, err) => {
        if(err){
            console.log(err)
            return false
        } 

        result.name = newContent.name
        return result.save().then((updatedTask, saveErr) => {

            if(saveErr){
                console.log(saveErr)
                return false
            } else {
                return updatedTask
            }
        })
    })
}


/*

acitivity


*/

// 1 - 4

module.exports.getTask = (taskID) => {
    return Task.findById(taskID).then(result => {
        return result
    })
}

// 5-8


module.exports.updateTaskStatus = (taskID, newContent) => {
    return Task.findById(taskID).then((result, err) => {
        if(err){
            console.log(err)
            return false
        } 

        result.status = newContent.status
        return result.save().then((updatedTask, saveErr) => {

            if(saveErr){
                console.log(saveErr)
                return false
            } else {
                return updatedTask
            }
        })
    })
}