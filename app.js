const express = require('express');
const taskRoutes = require('./routes/taskRoutes')

const mongoose = require('mongoose');




const app = express();


const port = 4000;


app.use(express.json())
	
app.use(express.urlencoded({extended: true}));

mongoose.connect('mongodb+srv://admin:admin123@zuitt-bootcamp.cpkli.mongodb.net/session30?retryWrites=true&w=majority', {

        useNewUrlParser: true,
        useUnifiedTopology: true
    });
let db = mongoose.connection;

 
    db.on('error', console.error.bind(console, "connection error"))
 
    db.once('open', () => console.log('Connected to the cloud database'))

    app.use('/tasks', taskRoutes)

    app.listen(port, () => console.log(`The Server is running at port ${port}`));