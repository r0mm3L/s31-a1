// contain all task endpoints for our applications

const express = require('express')
const router = express.Router()
const taskController = require('../controllers/taskControllers')

router.get('/', (req, res) => {

    taskController.getAllTasks().then(resultFromController => res.send 
        (resultFromController))
})


router.post('/createtask', (req, res) => {

    taskController.createTask(req.body).then(resultFromController => res.send 
        (resultFromController))
})


// router.post('/createtask', createTask.all);

// router.post('/createtask', createTask(req.body).create);

router.delete('/deleteTask/:_id', (req, res) => {
                                //req.params you are getting from the url like /:id
    taskController.deleteTask(req.params.id).then(resultFromController => res.send
        (resultFromController))
})

router.put('/updateTask/:id', (req, res) => {
    taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})


/*

activity


*/


// 1-4

router.get('/tasks/:id', (req, res) => {
    taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController))
})


// 5-8

router.put('/tasks/:id/complete', (req, res) => {
    taskController.updateTaskStatus(req.params.id, req.body).then(resultFromController => res.send(resultFromController))
})


module.exports = router